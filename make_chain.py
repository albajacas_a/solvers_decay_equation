import numpy as np
from bs4 import BeautifulSoup
from scipy.sparse import spmatrix
import queue

decay_file = '/data/user/albajacas_a/openmc-dev/chain_endfb71_pwr.xml'

def get_decay_matrix(nuclide, max_size = 10, verbose = False):
    '''
    Returns the triangular matrix A for the decay chain of an isotope.
    nuclide : string. Nuclide identification, e.g. "H7" or "Pu241"
    '''

    # Get data:
    with open(decay_file, 'r') as f:
        decay_chain = f.read()
    decay_chain = BeautifulSoup(decay_chain, 'xml')

    # Make the matrix:
    A = np.zeros((max_size, max_size))
    nucs = [nuclide]  # List of nuclides in matrix.
    q = queue.Queue()  # Queue of nuclides that still need to be added to the matrix.
    q.put(nuclide)
    col = 0  # Current column index.
    while (not q.empty()):
        # Make column for one nuclide:
        nuc = q.get()
        col = nucs.index(nuc)
        if col >= max_size:
            break
        if verbose:
            print("Writing nuclide", nuc)
        nuc = decay_chain.find('nuclide', {'name':nuc})
        decays = nuc.find_all('decay')
        if verbose:
            print("Which decays in the following ways:", decays)
        if len(decays) > 0:
            decay_rate = 1/float(nuc.get('half_life'))
            A[col,col] = -decay_rate
            for decay in decays:
                mode = decay.get('type')
                if mode == 'sf':
                    # We don't take into account decay by spontaneous fission (sf).
                    # Only account for alpha and beta+- decays.
                    continue
                if not mode in ['alpha', 'beta-', 'beta+', 'beta-,n']:
                    print("WARNING! Unknown decay mode", mode)
                target = decay.get('target')
                br = float(decay.get('branching_ratio'))
                if target in nucs:
                    row = nucs.index(target)
                else:
                    q.put(target)
                    nucs.append(target)
                    row = len(nucs)-1
                    
                if row < max_size:
                    A[row,col] = decay_rate * br

    if len(nucs) < max_size:
        A = A[:len(nucs),:len(nucs)]
    assert np.alltrue(A.diagonal() <= 0)
    return A, nucs[:A.shape[0]]
