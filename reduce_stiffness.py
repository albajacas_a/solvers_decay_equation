import numpy as np
from scipy.sparse import issparse

def reduce_stiffness(A, stiff):
    '''
    Returns matrix A, where the stiffness has been reduced.
    This is achieved by scaling the columns.
    Overall it is ensured that the stiffness ratio |λ_max| / |λ_min| = stiff.
    Furthermore, stable nuclides are removed from the matrix (i.e. nuclides with lambda = 0).
    
    A : matrix or sparse matrix. It is assumed that the eigvals of A are its diagonal!
    (i.e. A is assumed to be triangular or permutable to triangular)
    stiff : float. Desired stiffness
    '''

    A = A.toarray() if issparse(A) else A
    n = A.shape[0]

    A = remove_zeros(A)
    true_stiff = get_stiffness(A)
    if stiff >= true_stiff:
        print('required stiffness is larger than current stiffness. Returning A as is.')
        return A

    lams = np.abs(A.diagonal())
    lmax = lams.max()
    lmin = lams.min()
    lmax_new = lmin * stiff
    lams_scaled = (lams - lmin) / (lmax - lmin) * (lmax_new - lmin) + lmin
    scale_matrix = np.diag(lams_scaled / lams)
    
    # version 2: Instead of making all columns smaller, bring columns closer to the median.
    # We want to bring both very small and very large eigvals closer to the median eigval.
    # stiff = sqrt(stiff)^2 = lmax/lmin = lmax/lmed * lmed/lmin <= lmax/lmed = lmed/lmin = sqrt(stiff)
#     lmax = lams.max()
#     lmin = lams.min()
#     lmed = np.median(lams)
#     lmax_new = lmed * np.sqrt(stiff)
#     lmin_new = lmed / np.sqrt(stiff)
#     lams_scaled = (lams - lmin) / (lmax - lmin) * (lmax_new - lmin_new) + lmin_new
#     scale_matrix = np.diag(lams_scaled / lams)

    return np.matmul(A,scale_matrix)

def get_stiffness(A):
    "Returns stiffness of A"
    
    lams = np.abs(A.diagonal())
    lams = lams[lams != 0]
    return lams.max() / lams.min()

def remove_zeros(A):
    '''
    Removes columns and rows of A where there is a 0 in the diagonal.
    If A is diagonal or triangular, this is equivalent to removing columns
    that correspond to an eigenvalue 0.
    If additionally all the remaining diagonal entries are distinct, this 
    operation is equivalent to rendering A nonsingular.
    '''
    diag = A.diagonal()
    if not (0.0 in diag):
        return A
    
    return A[:, diag != 0][diag != 0, :]
