import numpy as np

# Script by Miha and Arnau, to permute matrix to lower triangular.

def triangularise_matrix(A, atol = 10**-20, max_tries = 20):
    '''
    Triangularises matrix with brute force permuations.
    A : ndarray. Square matrix that gets triangularised in place.
    atol : float. Tolerance used when checking if A is triangular.
    max_tries : int. Maximum number of times the algorithm runs through
    the entire matrix. If after max_tries A is still not triangular,
    it is considered non-triangularisable.

    Returns:
    idxs : list of ints, which corresponds to permutation vector.
    '''
    if A.shape[0] != A.shape[1]:
        print("Error! Matrix needs to be squared to be triangularised")
        return
    
    N = A.shape[0]
    idxs = np.arange(N)
    for tr in range(max_tries):
        for i in range(A.shape[0]):
            for j in range(i+1, A.shape[1]):
                if A[i,j] != 0:
                    A[[j,i]] = A[[i,j]]  # Swap rows.
                    A[:,[j,i]] = A[:,[i,j]]  # Swap columns.
                    idxs[[j,i]] = idxs[[i,j]]  # Swap idxs.
        if np.allclose(A, np.tril(A), atol=atol) == True:
            return idxs
    print("Matrix could not be triangularised after", max_tries, "runs.")