from make_chain import get_decay_matrix
import numpy as np
from scipy.sparse import csc_matrix, save_npz
np.set_printoptions(precision=2)

print(get_decay_matrix('U235', max_size = 10, verbose = True))

print(get_decay_matrix('Th227', verbose = False))

A, nucs = get_decay_matrix('Rn222', max_size = 25, verbose = False)
print(A)
print("Nuclides:", nucs)
lams = A.diagonal()
print("Lambdas:", lams, "\n")
lams = lams[lams != 0]  # Remove zeros for the stiffness calculation.
print("Stiffness: {:.2e}".format(np.min(lams) / np.max(lams)))
print("A size:", A.shape)
save_npz('Rn222_decay_n=17', csc_matrix(A))