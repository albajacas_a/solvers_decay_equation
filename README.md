# solvers_decay_equation

A few scripts for solving the decay equation, and some other helper scripts.

- CRAM based on [Pusa and Lepannen,2010](https://www.tandfonline.com/doi/abs/10.13182/NSE09-14). The python implementation is a copy-paste from the [opendeplete](https://github.com/mit-crpg/opendeplete/blob/master/opendeplete/integrator/cram.py) repository. 
  This method is accurate for matrices with eigenvalues that have negative real parts, and small imaginary parts. It remains accurate even if matrix is stiff, but has poor performance if eigenvalues have large imaginary parts.
- Padé approximation 
  This method is accurate for matrices with eigenvalues that are near the origin. It is not affected by whether the eigenvalues are real or imaginary, but has poor performance if matrix is stiff.
- Runge-Kutta order 4 with order 5 error-estimation (a.k.a. RK45)
  This method converges to the correct solution if the timestep is chosen small enough. The problem is that it might require extremely small timesteps that make it inefficient.
- make_chain.py : This script makes a decay matrix of an arbitrary size from the endfb71 decay data. It can be used to make matrices of varying stiffness and size.
- reduce_stiffness.py : This script is used to reduce the stiffness of a matrix by scaling all of its columns.
- triangularise_matrix.py : This script uses brute force column/row-swapping to triangularise a matrix. If the matrix cannot be triangularised via permutations it will print a warning.
- visualise_matrices.ipynb : This notebook is an example for visualising and triangularising matrices.

The repository also contains some files test_* that serve as unit tests for all of these functions/scripts.

Many of these examples work on decay/burnup matrices provided in merlin-l-002:/scratch/albajacas_a/A_matrices/

## Usage of the solvers

For a given solver, matrix `A`, final time `t`, and initial vector `N0`, you do
```
    solve(A,N0,t)
```
