import numpy as np
from PadeSolve import PadeSolve
from CRAMsolve import CRAM16
from RKsolve import RK45solve
from scipy.sparse import csc_matrix, issparse, save_npz, load_npz
import time
import sys

fn = sys.argv[1]
print("Reading A matrix from", fn)
A = load_npz(fn)
n = A.shape[0]
print("Matrix A:\n", A.toarray()) if n < 5 else print("Matrix A has size", A.shape)
print("\n")

N0 = np.ones(n)
t = 3.5
A = csc_matrix(A) if not issparse(A) else A  # Make A sparse if it wasn't already.

print("Using Padé approximation")
tic = time.perf_counter()
Nfpade = PadeSolve(A, N0, t, k = 0)
toc = time.perf_counter()
print(f"It took {toc - tic:0.4f} seconds")

print("Using Chebyshev approximation")
tic = time.perf_counter()
Nfcram = CRAM16(A, N0, t)
toc = time.perf_counter()
print(f"It took {toc - tic:0.4f} seconds")

print("Using RK45")
tic = time.perf_counter()
NfRK = RK45solve(A, N0, t, atol = 1e-14, rtol = 1e-13)
toc = time.perf_counter()
print(f"It took {toc - tic:0.4f} seconds")

print("\nComparisons:")
print("Pade vs Cram:")
err = np.abs((Nfpade - Nfcram) / Nfcram)
print("mean absolute relative error", np.mean(err))
print("max absolute relative error", np.max(err))
print("Pade vs RK45:")
err = np.abs((Nfpade - NfRK) / NfRK)
print("mean absolute relative error", np.mean(err))
print("max absolute relative error", np.max(err))
print("RK45 vs Cram:")
err = np.abs((NfRK - Nfcram) / Nfcram)
print("mean absolute relative error", np.mean(err))
print("max absolute relative error", np.max(err))

if n < 5:
    print("CRAM solution:", Nfcram)
    print("Padé solution:", Nfpade)
    print("RK solution:", NfRK)