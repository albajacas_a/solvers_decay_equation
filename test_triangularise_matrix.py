import numpy as np
import matplotlib.pyplot as plt
from triangularise_matrix import triangularise_matrix

N = 20
# A = np.random.rand(N,N)  # Non-triangular matrix.
A = np.tril(np.random.rand(N,N))

# Random permuation.
idxs1 = np.random.choice(N, size = int(N/2))
idxs2 = np.random.choice(N, size = int(N/2))

for k,_ in enumerate(idxs1):
    i,j = idxs1[k], idxs2[k]
    A[[j,i]] = A[[i,j]]  # Swap rows.
    A[:,[j,i]] = A[:,[i,j]]  # Swap columns.

fig, axs = plt.subplots(1,2,figsize = (10,5))
axs[0].set_title("Before")
axs[0].imshow(A, vmin = 0, vmax = .0001)

# Triangularise
idxs = triangularise_matrix(A)
print("permutation indexes:", idxs)
axs[1].set_title("After")
axs[1].imshow(A, vmin = 0, vmax = .0001)

plt.show()