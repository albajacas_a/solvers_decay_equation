"""
Solve the decay equation with Rational Padé approximation.

The equation is 
dN/dt = A*N(t) with N(0) = N0
and has the solution
N(t) = exp(A*t) * N0

Approximating exp(A*t) is nontrivial. Here we use the Padé approximation (expm function),
which works well when A has eigenvalues near the origin. Therefore this method fails
for stiff matrices.
"""

import numpy as np
from scipy.linalg import expm

def PadeSolve(A, N0, t, k = 0):
    assert A.shape[0] == A.shape[1], "A must be a square matrix"
    assert A.shape[0] == len(N0), "N0 must be the same length as A"

    if k == 0:
        return (expm(A * t)).dot(N0)
    else:
        print("Using scaling-squaring with k =", k)
        A = expm((A * t) / 2**k)
        for _ in range(k):
            A = A @ A  # Multiplication of sparse matrices.
        return A.dot(N0)